function initChat() {
	var WS = window['MozWebSocket'] ? MozWebSocket : WebSocket;
	var address = 'ws://' + window.location.host + '/chat/' + roomId + '/ws';

	chatSocket = new WS(address);

	chatSocket.onmessage = onMessage;
	chatSocket.onclose = onClose;
	chatSocket.onerror = onClose;
	chatSocket.onopen = onOpen;
}

function onOpen() {
    $("#sender input").prop('disabled', false);
    send({
        what: "update"
    })
}

function onClose() {
    $("#sender input").prop('disabled', true);
}

var persons = []

var sign = '&rsaquo; '

function onMessage(event) {
    var data = JSON.parse(event.data)

    switch (data.what) {
        case 'bye':
            window.location = 'http://' + window.location.host
            break
        case 'persons':
            persons = []
            for (g in data.persons) {
                addPerson(data.persons[g])
            }
            break
        case 'message':
            addLine('<span>' + data.who + '</span> &rarr; ' + data.said)
            break
        case 'info':
            if (data.to == null || data.to == personId) {
                addLine('<i>' + data.message + '</i>')
            }

    }
}

function addLine(line, style) {
    var b = '<li>'
    if (style != null) b='<li class="' + style + '">'
    var chat = $("ul#chat")
    chat.append(b + line + '</li>\n')
    chat.scrollTop(chat[0].scrollHeight)
}

function addPerson(d) {
    persons.push(d.name)
}

function send(obj) {
    chatSocket.send(JSON.stringify(obj));
}

function say(something) {
    if (something != null)
        send({
            what: 'say',
            message: String(something)
        })
}

function doSay() {
    var input = $('#sender input[type="text"]')[0]
    say(input.value)
    input.value = ''
}


function assignChat() {
    $('#sender input[type="button"]').click(function(event) {
        doSay()
        event.preventDefault()
    });

    $('#sender input[type="text"]').keypress(function(event) {
        if (event.charCode == 13 || event.keyCode == 13) {
            doSay()
            event.preventDefault()
        }
    })
}


$(function() {
    initChat()
    assignChat()
})

