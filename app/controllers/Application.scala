package controllers

import play.api._
import play.api.mvc._
import play.api.libs.json.JsValue
import actors.Chat
import akka.actor.{ActorRef, Props}
import play.api.libs.concurrent.Akka
import play.api.libs.iteratee._
import play.api.libs.iteratee.Concurrent.Channel
import play.api.data.Form
import play.api.data.Forms._
import actors.Chat._
  import scala.concurrent.ExecutionContext
import ExecutionContext.Implicits.global
import actors.Chat.PersonCame
import actors.Chat.PersonQuited
import actors.Chat.Message
import play.api.libs.json.JsString
import scala.Some
import actors.Chat.Person
import play.api.libs.json.JsObject

object Application extends Controller {

  case class ChatParams(name: String, password: String)

  val createForm = Form(
    mapping(
      "name" -> nonEmptyText,
      "password" -> text
    )(ChatParams.apply)(ChatParams.unapply)
  )

  def index = Action {
    Ok(views.html.index(chats.map(c => (c._1, c._2.name)).toList, createForm))
  }

  def create = Action { implicit request =>
    createForm.bindFromRequest.fold(
      errors => {
        Redirect(routes.Application.index())
      },
      data => {
        Logger.debug(s"create new room ${data.name}")
        val id = scala.util.Random.alphanumeric.take(20).mkString

        import play.api.Play.current

        val (out, channel) = Concurrent.broadcast[JsValue]
        val actor = Akka.system.actorOf(Props(classOf[Chat], channel), name = s"chat:$id")
        chats += id -> ChatData(data.name, data.password, actor, channel, out)

        Redirect(routes.Application.chat(id))
      }
    )
  }

  case class PersonParams(name: String)

  val personForm = Form(
    mapping(
      "name" -> nonEmptyText
    )(PersonParams.apply)(PersonParams.unapply)
  )

  def login = Action {
    Ok(views.html.login(personForm))
  }

  def enter = Action { implicit request =>
    personForm.bindFromRequest.fold(
      error => {
        Logger.debug("cannot parse person data")
        BadRequest(views.html.login(personForm))
      },
      person => {
        request.session.get("returnTo") match {
          case Some("chat") =>
            Redirect(routes.Application.chat(request.session.get("chatId").getOrElse("missingId"))).withSession("name" -> person.name)
          case _ =>
            Redirect(routes.Application.index()).withSession("name" -> person.name)
        }
      }
    )
  }

  private def chatPersonId(chatId: String) = s"room:$chatId:name"

  def chat(chatId: String) = Action { implicit request =>
    request.session.get("name").map {
      name =>
        val id = scala.util.Random.alphanumeric.take(20).mkString
        chats.get(chatId).fold(Redirect(routes.Application.index()))(chat =>
          Ok(views.html.chat(chat.name, chatId, name, id)).withSession(request.session + (chatPersonId(chatId) -> id))
        )
    }.getOrElse {
      Redirect(routes.Application.login()).withSession("returnTo" -> "chat", "chatId" -> chatId)
    }
  }

  def open(chatId: String) = WebSocket.using[JsValue] { implicit request =>
    val connect = for {
      name <- request.session.get("name")
      chat <- chats.get(chatId)
      personId <- request.session.get(chatPersonId(chatId))
    } yield {
      Logger.debug(s"new person ($name) coming to chat (${chat.name}})")
      //val personId = scala.util.Random.alphanumeric.take(20).mkString
      chat.actor ! PersonCame(personId, Person(name))
      (person(personId, chat.actor), chat.enumerator)
    }

    connect.getOrElse {
      val in = Done[JsValue, Unit]((), Input.EOF)
      val out = Enumerator[JsValue](JsObject(Seq("what" -> JsString("bye")))).andThen(Enumerator.eof)
      (in, out)
    }
  }

  private var chats = scala.collection.mutable.Map[String, ChatData]()

  case class ChatData(name: String, password: String, actor: ActorRef, channel: Channel[JsValue], enumerator: Enumerator[JsValue])

  private def person(personId: String, actor: ActorRef): Iteratee[JsValue, Unit] = Iteratee.foreach[JsValue] {
    message =>
      val key = (message \ "what").asOpt[String]
      key match {
        case Some("say") =>
          actor ! Message(personId, (message \ "message").asOpt[String].getOrElse("???"))
        case Some("update") =>
          actor ! RequestPersons
        case _ =>
          Logger.debug(s"unknown message: ${key.getOrElse("????")}")
      }
  }.map {
    _ =>
      actor ! PersonQuited(personId)
  }

}