package actors

import akka.actor.Actor
import play.api.libs.json._
import actors.Chat._
import play.api.libs.iteratee.Concurrent.Channel
import actors.Chat.PersonCame
import actors.Chat.PersonQuited
import actors.Chat.Person
import actors.Chat.Message
import actors.Chat.PersonCame
import actors.Chat.PersonQuited
import actors.Chat.Person
import actors.Chat.Message
import play.api.libs.json.JsObject
import play.api.libs.json.JsString

/**
 *
 * User: mateusz
 * Date: 10.05.2014
 * Time: 21:41
 * Created with IntelliJ IDEA.
 */
class Chat(channel: Channel[JsValue]) extends Actor {

  var persons = scala.collection.mutable.Map[String, Person]()

  def nameResolver(personId: String) = persons.get(personId).fold("???")(_.name)

  override def receive: Receive = {
    case message: Message =>
      channel.push(message.json(nameResolver))
    case PersonCame(id, person) =>
      persons += id -> person
      channel.push(JsObject(Seq(
        "what" -> JsString("personCame"),
        "who" -> person.json
      )))
    case PersonQuited(id: String) =>
      val who = persons.get(id)
      persons -= id
      who.map {
        person =>
          channel.push(JsObject(Seq(
            "what" -> JsString("personQuited"),
            "who" -> person.json
          )))
      }.getOrElse()
    case RequestPersons =>
      channel.push(JsObject(Seq(
        "what" -> JsString("persons"),
        "persons" -> JsArray(
          persons.map(_._2.json).toSeq
        )
      )))
    case _ =>
  }
}

object Chat {
  case class Message(who: String, said: String) {
    def json(nameResolver: String => String) = JsObject(Seq(
      "what" -> JsString("message"),
      "who" -> JsString(nameResolver(who)),
      "said" -> JsString(said)
    ))
  }
  case class Person(name: String) {
    lazy val json = JsObject(Seq(
      "name" -> JsString(name)
    ))
  }
  case class PersonCame(id: String, person: Person)
  case class PersonQuited(id: String)

  case object RequestPersons
}
